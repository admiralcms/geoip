<?php
  namespace Admiral\GeoIP;

  use Cake\Core\Configure;
  use GeoIp2\Database\Reader;
  use Cake\Filesystem\File;

  class ASN {
    protected $dir;
    protected $reader;

    public function __construct(string $database) {
      // Make sure a directory is set in the config
      if(!$this->dir = Configure::read('GeoIP.dir')) {
        throw new \Exception('No directory has been specified in the config.');
      }

      // Make sure the database exists
      $file = new File($this->dir . $database);
      if(!$file->exists()) {
        throw new \Exception('The specified database does not exist at "' . $file->path . '"');
      }

      // Open our reader
      $this->reader = new Reader($file->path);
    }

    /**
     * Returns our reader
     * 
     * @return \GeoIp2\Database\Reader;
     */
    public function getReader(): Reader {
      return $this->reader;
    }
  }
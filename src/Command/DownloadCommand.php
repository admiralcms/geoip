<?php
  namespace Admiral\GeoIP\Command;

  use Cake\Console\Arguments;
  use Cake\Console\Command;
  use Cake\Console\ConsoleIo;
  use Cake\Console\ConsoleOptionParser;
  use Cake\Http\Client;
  use Cake\Core\Configure;
  use Cake\Filesystem\Folder;
  use Cake\Filesystem\File;

  class DownloadCommand extends Command {
    protected $baseUrl = 'https://download.maxmind.com/app/geoip_download';
    protected $licenseKey;
    protected $http;

    /**
     * 
     */
    public function initialize() {
      // Initialize our parent first
      parent::initialize();

      // Load our licensekey
      // Throw an error if it's not set
      if(!$this->licenseKey = Configure::read('GeoIP.license')) {
        throw new \Exception("GeoIP License key has not been set in the config but is required!");
      }

      // Initialize an HTTP client
      $this->http = new Client();
    }

    /**
     * 
     */
    protected function buildOptionParser(ConsoleOptionParser $parser) {
      $parser
        ->addOption('asn', [
          'help' => 'Include the ASN database',
          'boolean' => true,
        ])
        ->addOption('city', [
          'help' => 'Include the City database',
          'boolean' => true,
        ])
        ->addOption('country', [
          'help' => 'Include the Country database',
          'boolean' => true,
        ]);

      return $parser;
    }

    /**
     * 
     */
    public function execute(Arguments $args, ConsoleIo $io) {
      if($args->getOption('asn')) $this->downloadAsn($io);
      if($args->getOption('city')) $this->downloadCity($io);
      if($args->getOption('country')) $this->downloadCountry($io);
    }

    /**
     * 
     */
    protected function downloadAsn(ConsoleIo $io) {     
      // Open a handle to the download file
      $tmp = new File(TMP . '/geolite2-asn.tar.gz', true);

      // Download the file to our buffer
      $io->out("Downloading ASN database...");
      $res = $this->http->get($this->baseUrl, [
        'edition_id' => 'GeoLite2-ASN',
        'license_key' => $this->licenseKey,
        'suffix' => 'tar.gz',
      ]);
      
      // Write each 4096 bytes to the file
      while (!$res->getBody()->eof()) {
        $tmp->append($res->getBody()->read(4096));
      }

      // Response is no longer needed
      // Destroy it to save memory
      unset($res);

      // Unpack the file
      $io->out("Extracting ASN database...");
      $p = new \PharData($tmp->path);
      $p->extractTo(TMP . '/geolite2-asn');

      // Open a handle to the folder
      $folder = new Folder(TMP . '/geolite2-asn');

      // Find our database file
      $files = $folder->findRecursive('GeoLite2-ASN.mmdb');

      // Make sure our file was found
      if(!$files) throw new \Exception('No database file was found!');

      // File was found
      // Move it to it's final destination
      \rename($files[0], Configure::read('GeoIP.dir') . '/GeoLite2-ASN.mmdb');

      // Clean up
      $io->out("Cleaning up a bit...");
      $tmp->delete();
      $folder->delete();
    }

    /**
     * 
     */
    protected function downloadCity(ConsoleIo $io) {
      // Open a handle to the download file
      $tmp = new File(TMP . '/geolite2-city.tar.gz', true);

      // Download the file
      $io->out("Downloading City database...");
      $res = $this->http->get($this->baseUrl, [
        'edition_id' => 'GeoLite2-City',
        'license_key' => $this->licenseKey,
        'suffix' => 'tar.gz',
      ]);
      
      // Write each 4096 bytes to the file
      while (!$res->getBody()->eof()) {
        $tmp->append($res->getBody()->read(4096));
      }

      // Response is no longer needed
      // Destroy it to save memory
      unset($res);

      // Unpack the file
      $io->out("Extracting City database...");
      $p = new \PharData($tmp->path);
      $p->extractTo(TMP . '/geolite2-city');

      // Open a handle to the folder
      $folder = new Folder(TMP . '/geolite2-city');

      // Find our database file
      $files = $folder->findRecursive('GeoLite2-City.mmdb');

      // Make sure our file was found
      if(!$files) throw new \Exception('No database file was found!');

      // File was found
      // Move it to it's final destination
      \rename($files[0], Configure::read('GeoIP.dir') . '/GeoLite2-City.mmdb');

      // Clean up
      $io->out("Cleaning up a bit...");
      $tmp->delete();
      $folder->delete();
    }

    /**
     * 
     */
    protected function downloadCountry(ConsoleIo $io) {
      // Open a handle to the download file
      $tmp = new File(TMP . '/geolite2-country.tar.gz', true);

      // Download the file to our buffer
      $io->out("Downloading Country database...");
      $res = $this->http->get($this->baseUrl, [
        'edition_id' => 'GeoLite2-Country',
        'license_key' => $this->licenseKey,
        'suffix' => 'tar.gz',
      ]);
      
      // Write each 4096 bytes to the file
      while (!$res->getBody()->eof()) {
        $tmp->append($res->getBody()->read(4096));
      }

      // Response is no longer needed
      // Destroy it to save memory
      unset($res);

      // Unpack the file
      $io->out("Extracting Country database...");
      $p = new \PharData($tmp->path);
      $p->extractTo(TMP . '/geolite2-country');

      // Open a handle to the folder
      $folder = new Folder(TMP . '/geolite2-country');

      // Find our database file
      $files = $folder->findRecursive('GeoLite2-Country.mmdb');

      // Make sure our file was found
      if(!$files) throw new \Exception('No database file was found!');

      // File was found
      // Move it to it's final destination
      \rename($files[0], Configure::read('GeoIP.dir') . '/GeoLite2-Country.mmdb');

      // Clean up
      $io->out("Cleaning up a bit...");
      $tmp->delete();
      $folder->delete();
    }

    protected function convert($size) {
      $unit=array('b','kb','mb','gb','tb','pb');
      return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
  }
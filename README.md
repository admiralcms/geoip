# GeoIP
Admiral plugin to get GeoIP data through the MaxMind GeoIP2 database

# Getting Started
First, install the plugin using Composer

> composer require admiral/geoip

Next, load the plugin into your `Application.php`:
```php
class Application extends BaseApplication {
  public function bootstrap() {
    parent::bootstrap();

    // Load the GeoIP plugin
    $this->addPlugin('Admiral/GeoIP');
  }
}
```

Add some configs to your `config/app.php`:
```php
'GeoIP' => [
  'license' => 'YOUR_LICENSE_KEY',
  'dir' => TMP, // Or the directory you desire
],
```

Download the databases of choice:

> bin/cake geoip download --asn --city --country

Loading a database:
```php
use Admiral\GeoIP\Database;

$asnDb = new Database('GeoLite2-ASN.mmdb')->getReader();
```

After which it serves identically to [the library from MaxMind](https://github.com/maxmind/GeoIP2-php#database-reader).

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | CakePHP Version |
|----------------|-----------------|
| 1.x            | >=3.6           |